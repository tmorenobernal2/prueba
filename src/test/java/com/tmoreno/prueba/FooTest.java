package com.tmoreno.prueba;

import org.junit.Assert;
import org.junit.Test;

public class FooTest {
    
    @Test
	public void okTest() {
        Foo foo = new Foo();
        
        foo.foo();
        
        Assert.assertTrue(foo.foo());
    }
    
    @Test
	public void errorTest() {
        Foo foo = new Foo();
        
        foo.foo();
        
        Assert.assertFalse(!foo.foo());
    }
    
    @Test
	public void test2() {
        Foo foo = new Foo();
        
        foo.foo();
        
        Assert.assertTrue(foo.foo());
    }
    
}